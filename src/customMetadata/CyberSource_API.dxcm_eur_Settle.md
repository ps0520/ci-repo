<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Settle</label>
    <protected>false</protected>
    <values>
        <field>CurrencyIsoCode__c</field>
        <value xsi:type="xsd:string">EUR</value>
    </values>
    <values>
        <field>Enviornment__c</field>
        <value xsi:type="xsd:string">Prod</value>
    </values>
    <values>
        <field>endPoint__c</field>
        <value xsi:type="xsd:string">https://api.cybersource.com/pts/v2/payments/{PaymentId}/captures</value>
    </values>
    <values>
        <field>host__c</field>
        <value xsi:type="xsd:string">api.cybersource.com</value>
    </values>
    <values>
        <field>keyid__c</field>
        <value xsi:type="xsd:string">d6e4c998-431c-4bc1-a6aa-0977a08aeed7</value>
    </values>
    <values>
        <field>merchant_id__c</field>
        <value xsi:type="xsd:string">dxcm_eur</value>
    </values>
    <values>
        <field>profile_id__c</field>
        <value xsi:type="xsd:string">5981FAB4-A13C-4262-A63C-44AE3E4DA672</value>
    </values>
    <values>
        <field>reqBody__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>secretKey__c</field>
        <value xsi:type="xsd:string">mHutEyC0RxpZbqGdLKLSWQWB7sVsnrMuUy6y3pdRQwU=</value>
    </values>
</CustomMetadata>
